# Codebase State
- [x] Licenses
    * [x] reuse compliant
- [x] .editorconfig
- [x] Changelog
- [x] Static Analysis
    * [x] Code formatting
    * [x] Lint
    * [ ] Cyclomatic complexity
- [ ] Dynamic Analysis
- [ ] Unit tests
- [x] Integration tests
- [ ] Coverage report generation
- [x] Code quality report
- [x] Online documentation
- [x] Release management
- [x] Packaging
    * [X] Ubuntu package
    * [ ] PYPI package
- [x] Test deployed package
