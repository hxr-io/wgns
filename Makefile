# Makefile for wgns
# SPDX-FileCopyrightText: 2023 HXR LLC <code@hxr.io>
# SPDX-License-Identifier: Apache-2.0 OR MIT

PROG ?= wgns
PREFIX ?= /usr
DESTDIR ?=
MANDIR ?= $(PREFIX)/share/man

all:
	@echo "Ready to install!"

lint:
	shellcheck -x --color $(PROG)

format:
	shfmt --binary-next-line --diff $(PROG)

mkdocs:
	mkdocs serve --strict --verbose

install:
	@install -v -d "$(DESTDIR)$(MANDIR)/man1" && install -m 0644 -v wgns.1 "$(DESTDIR)$(MANDIR)/man1/wgns.1"
	@install -v -d "$(DESTDIR)$(PREFIX)/bin/"
	@install -v -m0755 wgns "$(DESTDIR)$(PREFIX)/bin/wgns"
	@echo
	@echo "wgns is installed succesfully"
	@echo

