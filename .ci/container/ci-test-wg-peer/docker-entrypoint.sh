#!/bin/ash
set -eo pipefail

mkdir -p /etc/wireguard
envsubst < /usr/local/share/wgns/wg0.conf.template > /etc/wireguard/wg0.conf

ip link add dev wg0 type wireguard
ip address add dev wg0 $WG_FIXTURE_IP
wg setconf wg0 /etc/wireguard/wg0.conf
ip link set wg0 up

while true
do
	echo ${CI_JOB_ID} | nc -l $NETCAT_FIXTURE_PORT
done
