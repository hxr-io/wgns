#shellcheck shell=sh
Describe 'wgns'
  Describe 'integration'
    Skip if 'CI pipeline not enabled' [ ! "${CI}" = "true" ]
    It 'ping fixture via WireGuard'
      When run command ping -c1 ${WG_FIXTURE_IP}
      The output should include '1 received'
      The status should be success
    End
    It 'validate data transfer via WireGuard'
      When run command nc -d -N ${WG_FIXTURE_IP} 1234
      The output should equal "${CI_JOB_ID}"
      The status should be success
    End
  End
End
